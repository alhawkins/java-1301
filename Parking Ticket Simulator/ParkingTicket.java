
public class ParkingTicket
{
   private ParkedCar parkedCar;      
   private ParkingMeter parkingMeter;  
   private PoliceOfficer policeOfficer;      
   private int minutes;
	private int fine; 
	private int paidMinutes;
	
   public ParkingTicket(ParkedCar car, ParkingMeter Meter, PoliceOfficer officer)
   {
      parkedCar = new ParkedCar(car);
		parkingMeter = new ParkingMeter(Meter);
	   policeOfficer = new PoliceOfficer(officer);
   }
   
 	public ParkingTicket(ParkingTicket object2)
	{
		parkedCar = object2.parkedCar;
  		parkingMeter = object2.parkingMeter;    
		policeOfficer = object2.policeOfficer;
	}    
	
	
	
	public void fine(int f)
	{
		int minutes = f;
		
		if (minutes>0&&minutes<=60)
		{	
			fine = 25;	
		}
		else if (minutes>60)
		{
			int m = minutes%60; 
			int h = minutes/60;
			fine = 25;
			fine += (h-1)*10;
			if (m>0)
				fine += 10;				
		}
	}	  
	
   public String toString()
   {
     
      String str = parkedCar+"\n"+parkingMeter+"\n"+policeOfficer+
										"\nYour fine is:           $"+fine;

     
      return str;
   }
}