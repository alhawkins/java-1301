/**
 	Author: Allan Hawkins 
	Class: CSCI 1302  
	The Employee class holds
   general data about an Employee. 
*/

public class Employee
{
   private String name;       // Employee name
   private String idNumber;   // Employee ID
   private String hireDate;  // hire date

   /**
      The Constructor sets the employee's name,
      ID number, and hire date.
      @param n The employee's name.
      @param id The employee's ID number.
      @param date The date the empolyee was hired.
   */

   public Employee(String n, String id, String date)
   {
      name = n;
      idNumber = id;
      hireDate = date;
   }

   /**
      The toString method returns a String containing
      the employee's data.
      @return A reference to a String.
   */

   public String toString()
   {
      String str;

      str = "Name: " + name
         + "\nID Number: " + idNumber
         + "\nDate Hired: " + hireDate;
      return str;
   }

}