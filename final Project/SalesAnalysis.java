import java.io.*;
import javax.swing.JOptionPane;
import java.util.StringTokenizer;
import java.text.DecimalFormat;

/**
Author: Allan Hawkins 
Class: 1301, Prof. Bota
1301 Final Project
*/

/**
This program will open up a file and process it in the following way:

- Calculate the total sales for each week
- Calculate the average daily sales for each week
- Calculate the total sales for all the weeks
- Calculate the average weekly sales 
- Determine the week number that had the highest amount of sales 
- Determine the week number that had the lowest amount of sales 

Also, it will open up a second file where it will copy the processed information.
*/

public class SalesAnalysis
{
 	private String filename; // holds the name of the file to be processed
	private String filename2; // holds the name of the file where the processed information will be copied 
	private FileReader freader; // reference variable for the FileReader class 
 	private FileWriter fwriter;  // reference variable for the FileWriter class
	private BufferedReader inputFile; // refrence variable for the BufferedReader class
 	private PrintWriter outputFile; // reference variable for the printWriter class 
	private String line; // holds the line read in the file to be processed
		
	/**
	Constructor that receives the names of the file to be process and file where the processed 
	information will be copied
	@ param fn A string contaning the file to be processed
	@ param fn2 A string contaning the file where the processed information will be copied 
	*/
	
	public SalesAnalysis(String fn, String fn2)throws IOException
   {
      filename = fn;
		filename2 = fn2;
   }
   	
 	/**
	 getTotalWeek Method
	 Calculates the total sales for each week
	*/
	
	public void getTotalWeek()throws IOException
   {
      String testToken; // holds token
      double total = 0; // holds the total for week  
		int weekCounter = 1; // week counter
		
		DecimalFormat fmt = new DecimalFormat("#,###.00");
      
		freader = new FileReader(filename); // opens the file to be processes 
      inputFile = new BufferedReader(freader); // reads the file opened 
		fwriter = new FileWriter(filename2); // opens the file where the processed info. will be stored
		outputFile = new PrintWriter(fwriter); // prints the processed info. in the file 
		
		line = inputFile.readLine(); // reads first line of the input file and stores info. in line variable
		
		
		// loop that calculates the totals for each week 
		while(line != null)
		{
		// creats StringTokenizer object passes the line variable as an argument and sets a comma as a delimiter
		StringTokenizer tokenizer = new StringTokenizer(line, ",");
    
   		// loop that process all the tokens of the string received   	
			while (tokenizer.hasMoreTokens())
      	{
       		  testToken = tokenizer.nextToken();// equals the token read to the variable testToken 
        		  total += Double.parseDouble(testToken);// adds the testToken variables
      
			}
    
      
 		// shows the processed information through a popup window
		JOptionPane.showMessageDialog(null, "Total for week " +
                            weekCounter + " is $" +
                            fmt.format(total));
		
		// prints the processed information to the output file 
		outputFile.println("Total for week " +
                            weekCounter + " is $" +
                            fmt.format(total));
		total = 0; // returns the total variable to zero 
		line = inputFile.readLine(); // reads next line 
		weekCounter++; // adds one to week counter 
		}
	inputFile.close(); // closes input file 
   }  
	
	/**
	 getDailyWeekAverage Method
	 Calculates the average daily sales for each week
	*/

	public void getDailyWeekAverage()throws IOException
   {
      String testToken; // holds token
      double total = 0; // holds the total for week 
		double count = 0; // loop counter  
      double average; // holds the daily average  
		int weekCounter = 1; // week counter 
		
		DecimalFormat fmt = new DecimalFormat("#,###.00");
      
		freader = new FileReader(filename);// opens the file to be processes 
      inputFile = new BufferedReader(freader);// reads the file opened 
		
		line = inputFile.readLine();// reads first line of the input file and stores info. in line variable
		
		System.out.println();// prints a blank line on the screen to separate the information 
		outputFile.println();// prints a blank line in the processed info. file to separate the information
		
		// loop that calculates the average daily sales for each week
		while(line != null)
		{
		// creats StringTokenizer object passes the line variable as an argument and sets a comma as a delimiter
		StringTokenizer tokenizer = new StringTokenizer(line, ",");
    
      // loop that process all the tokens of the string received     
		while (tokenizer.hasMoreTokens())
      {
         testToken = tokenizer.nextToken();// equals the token read to the variable testToken 
         total += Double.parseDouble(testToken);// adds the testToken variables
         count++;// adds one to counter
			
      }
    
      average = total / count;// divides total by count to get the average and equals it to the variable average
 		
		// shows the processed information through a popup window
		JOptionPane.showMessageDialog(null,"Average for week " + weekCounter + " is $" +
                            fmt.format(average));
		// prints the processed information to the output file
		outputFile.println("Average for week " + weekCounter + " is $" +
                            fmt.format(average));
		total = 0;// returns the total variable to zero
		count = 0;// returns the count variable to zero
		line = inputFile.readLine();// reads next line 
		weekCounter++; // adds one to connter 
		}
	inputFile.close();  // closes input file 
	}
   
 	/**
	 getTotalWeeks Method
	 Calculates the total sales for all the weeks
	*/
	
	
	public void getTotalWeeks()throws IOException
   {
      String testToken; // holds token
  		double weekTotal = 0; // holds the total for week   
		double totalAllWeeks = 0;// holds the total sales for all the weeks
		
		DecimalFormat fmt = new DecimalFormat("#,###.00");
      
		freader = new FileReader(filename);// opens the file to be processes 
      inputFile = new BufferedReader(freader);// reads the file opened 
		
		line = inputFile.readLine();// reads first line of the input file and stores info. in line variable
		
		System.out.println();// prints a blank line on the screen to separate the information 
		outputFile.println();// prints a blank line in the processed info. file to separate the information
		
		// loop that calculates the average daily sales for each week
		while(line != null)
		{
				// creats StringTokenizer object passes the line variable as an argument and sets a comma as a delimiter
				StringTokenizer tokenizer = new StringTokenizer(line, ",");
    
   	      // loop that process all the tokens of the string received 	   
				while (tokenizer.hasMoreTokens())
    			{
       			  testToken = tokenizer.nextToken();// equals the token read to the variable testToken 
         		  weekTotal += Double.parseDouble(testToken);// adds the testToken variables
      		}			
		totalAllWeeks += weekTotal;// adds the weekTotal variables
		weekTotal = 0;// returns weekTotal to zero
		line = inputFile.readLine();// reads next line in the input file
		}
		
		// shows the processed information through a popup window
		JOptionPane.showMessageDialog(null,"Total of weekly sales is $" + fmt.format(totalAllWeeks));
		// prints the processed information to the output file
		outputFile.println("Total of weekly sales is $" + fmt.format(totalAllWeeks));
		
		
		inputFile.close(); // closes input file  
	}
   
	/**
	 getWeekAverage Method
	 Calculates the average weekly sales 
	*/

	public void getWeekAverage()throws IOException
   {
      String testToken; // holds token
      double total = 0; // holds the total for week   
      double weekAverage = 0;// holds average for week  
		int weekCounter = 0; // counter
		
		DecimalFormat fmt = new DecimalFormat("#,###.00");
      
		freader = new FileReader(filename);// opens the file to be processes 
      inputFile = new BufferedReader(freader);// reads the file opened 
		
		line = inputFile.readLine();// reads first line of the input file and stores info. in line variable
		
		System.out.println();// prints a blank line on the screen to separate the information 
		outputFile.println();// prints a blank line in the processed info. file to separate the information
		
		//loop that calculates the average weekly sales 
		while(line != null)
		{
		// creats StringTokenizer object passes the line variable as an argument and sets a comma as a delimeter
		StringTokenizer tokenizer = new StringTokenizer(line, ",");
    
         // loop that process all the tokens of the string received    	
			while (tokenizer.hasMoreTokens())
      	{
       	     testToken = tokenizer.nextToken();// equals the token read to the variable testToken 
       	 	  total += Double.parseDouble(testToken);	// adds the testToken variables
      	}
    
      weekAverage += total;// adds the total variable and equals it to the weekAverge variable	
		total = 0;// returns the total variable to zero
		line = inputFile.readLine();// reads next line 
		weekCounter++;// adds one to weekCounter 
		}
		
		// shows the processed information through a popup window
		JOptionPane.showMessageDialog(null,"Average of weekly sales is $" + fmt.format(weekAverage/weekCounter));
		// prints the processed information to the output file
		outputFile.println("Average of weekly sales is $" + fmt.format(weekAverage/weekCounter));
	total = 0;	// returns the total variable to zero
	inputFile.close();  // closes input file  
	}

	/**
	 getHighestWeek Method
	 Determines the week number that had the highest amount of sales  
	*/

	public void getHighestWeek()throws IOException
   {
      String testToken;// holds token
  		double total = 0;	// holds the total for week 	
		double finalHighest = 0; // holds highest sales week   
		double highest = 0; // variable used to find the highest 
		double nextHighest = 0;// variable used to find the highest 
		int week = 0; // holds week number of the highest sales week
		int weekCounter = 0; // counter
		
		DecimalFormat fmt = new DecimalFormat("#,###.00");
      
		freader = new FileReader(filename);// opens the file to be processes 
      inputFile = new BufferedReader(freader);// reads the file opened 
		
		line = inputFile.readLine();// reads first line of the input file and stores info. in line variable
				
		System.out.println();// prints a blank line on the screen to separate the information 
		outputFile.println();// prints a blank line in the processed info. file to separate the information
		
		//loop the determines the week number that had the highest amount of sales  
		while(line != null)
		{
				// creats StringTokenizer object passes the line variable as an argument and sets a comma as a delimiter
				StringTokenizer tokenizer = new StringTokenizer(line, ",");
				
				// loop that process all the tokens of the string received 	  
			   while (tokenizer.hasMoreTokens())
    			{
     				testToken = tokenizer.nextToken();// equals the token read to the variable testToken 
         		total += Double.parseDouble(testToken);  // adds the testToken variables														
				}
		
		highest = total;// adds the total variable and equals it to highest variable
		
		// loop that determines the highest amount and the week number where it was found
		if (highest > nextHighest)  
      			{
						nextHighest = highest;// equals highest to nextHighest
						weekCounter++;//adds one to counter
						week = weekCounter;// equals weekCounter to week variable
						total = 0;// returns total variable to zero			
					}
					else
					{
						weekCounter++;//adds one to counter
						total = 0;// returns total variable to zero
					}
		finalHighest = nextHighest;// equals nextHighest to finalHighest			
		line = inputFile.readLine();// reads next line
		}
		
		// shows the processed information through a popup window
		JOptionPane.showMessageDialog(null,"Week " + week + " had the highest sales with $"+ fmt.format(finalHighest));
		// prints the processed information to the output file
		outputFile.println("Week " + week + " had the highest sales with $"+ fmt.format(finalHighest));
		
		inputFile.close(); // closes input file  
	}
  
	/**
	 getLowestWeek Method
	 Determines the week number that had the lowest amount of sales 
	*/
	
	public void getLowestWeek()throws IOException
   {
      String testToken; // holds token
  		double total = 0;	// holds the total for week 	
		double finalLowest = 0; // holds lowest sales week   
		double lowest = 0;// variable used to find the lowest 
		double nextLowest = 100000000;// variable used to find the lowest
		int week = 0;// holds week number of the lowest sales week
		int weekCounter = 0;//counter
		
		DecimalFormat fmt = new DecimalFormat("#,###.00");
      
		freader = new FileReader(filename);// opens the file to be processes 
      inputFile = new BufferedReader(freader);// reads the file opened 
		
		line = inputFile.readLine();// reads first line of the input file and stores info. in line variable
				
		System.out.println();// prints a blank line on the screen to separate the information 
		outputFile.println();// prints a blank line in the processed info. file to separate the information
		
		//loop that determines the week number that had the lowest amount of sales 
		while(line != null)
		{
			   // creats StringTokenizer object passes the line variable as an argument and sets a comma as a delimiter
				StringTokenizer tokenizer = new StringTokenizer(line, ",");
				
				// loop that process all the tokens of the string received 		  
			   while (tokenizer.hasMoreTokens())
    			{
     				testToken = tokenizer.nextToken();// equals the token read to the variable testToken
         		total += Double.parseDouble(testToken); // adds the testToken variables	 													
				}
		
		
		lowest = total;// equals total to lowest
		
					// loop that determines the lowest amount and the week number where it was found
					if (lowest < nextLowest)  
      			{
						nextLowest = lowest;// equals lowest to nextlowest
						weekCounter++;	// adds one to counter
						week = weekCounter;// equals counter to week 	
						total = 0;// returns total to zero
					}
					else
					{
						weekCounter++;	// adds one to counter
						total = 0;// returns total to zero
					}	

		finalLowest = nextLowest;// equals nextlowest to finalLowest			
		line = inputFile.readLine();// reads next line 
		}
		
		// shows the processed information through a popup window
		JOptionPane.showMessageDialog(null,"Week " + week + " had the lowest sales with $"+ fmt.format(finalLowest));
		// prints the processed information to the output file
		outputFile.println("Week " + week + " had the lowest sales with $"+ fmt.format(finalLowest));
		
		inputFile.close(); // closes th input file 
	}
  
  	/**
	 close Method
	 closes input and output files
	*/

	public void close() throws IOException
   {
		inputFile.close();
		outputFile.close();
   }
}