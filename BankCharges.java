public class BankCharges
{
	private double balance;
	private int writtenChecks;
	private double fees;
	


	public BankCharges()
	{
		balance = 0.0;
		writtenChecks = 0;
		
	}
	
	public BankCharges(double initialBalance, int checks )
	{
		balance = initialBalance;
		writtenChecks = checks;
	}
	
	public BankCharges(String b, String c)
	{
		balance = Double.parseDouble(b);
		writtenChecks = Integer.parseInt(c);
	}
	
	public void setBalance(double b)
	{
		balance = b;
	}
	
	public void setWrittenChecks(int c)
	{
		writtenChecks = c;
	}

	public void setFees(double f)
	{
		fees = f;
	}

	public void extraFee(double balance)
	{
		if (balance >= 400)
		{
			fees += 10;
		}
	
		else if (balance < 400 && balance > 0)
		{		
			fees += 25;
		}

	}
	public void fees(double checks)
	{
	
		
		if(writtenChecks < 20 && writtenChecks > 0)
							{
								fees += (writtenChecks * .10);
							} 
			
			
		else if(writtenChecks >= 20 && writtenChecks <= 39)
							{
								fees += (writtenChecks * .08);
							}
			
		else if(writtenChecks >= 40 && writtenChecks <=59)
							{
								fees += (writtenChecks * .06);
							}
		
		else if(writtenChecks >= 60)
							{
								fees += (writtenChecks * .04);
							}
		
	}
	
		
	public double getBalance()
	{
		return balance;
	}
	
	public double getFees()
	{
		return fees;
	}
	public int getWrittenChecks()
	{ 	
		return writtenChecks;
	}		

	
}