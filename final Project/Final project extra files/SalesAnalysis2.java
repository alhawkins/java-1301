import java.io.*;
import java.util.StringTokenizer;
import java.text.DecimalFormat;

public class SalesAnalysis2
{
 	private String filaname;  
	private String filename2;
	private FileReader freader;
 	private FileWriter fwriter;  
	private BufferedReader inputFile;
 	private PrintWriter outputFile;  
	private String line;
	private int weekCounter;
   private boolean lineRead;  
 	
	public SalesAnalysis2()
	{
		weekCounter = 0;
		lineRead = true;
	}  
	
	public SalesAnalysis2(String filename, String filename2)throws IOException
   {
      freader = new FileReader(filename);
      inputFile = new BufferedReader(freader);
		fwriter = new FileWriter(filename2);
		outputFile = new PrintWriter(fwriter);
   }
   
   
   public boolean readNextLine() throws IOException
   {
       
    
      line = inputFile.readLine();

		
      if (line != null)
    	{    
		  lineRead = true;
		  weekCounter++;
  		}    
		else
      {  
			lineRead = false;
  		}    
		return lineRead;
   }
	
   public double getAverage()throws IOException
   {
      String testToken; 
      double total = 0; 
		double count = 0;   
      double average;  
		
		DecimalFormat fmt = new DecimalFormat("#.00");
      StringTokenizer tokenizer = new StringTokenizer(line, ",");
    
       
		
      while (tokenizer.hasMoreTokens())
      {
         testToken = tokenizer.nextToken();
         total += Double.parseDouble(testToken);
         count++;
			
      }
    
      average = (double) total / count;
 		
			
		outputFile.println("Average for week " +
                            weekCounter + " is " +
                            fmt.format(average));
		return average;
   }
   
      
   public void close() throws IOException
   {
		inputFile.close();
		outputFile.close();
   }
}