import java.util.Scanner;
import java.text.DecimalFormat;

public class AccountTest
{

	public static void main(String[] args)
	{
		double initialBalance;
		double interestRate;
		int months;
		double deposit;
		double withdrawal;
		
		DecimalFormat dollar = new DecimalFormat("#,###.00");
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("What is your annual interest rate? ");
		interestRate = keyboard.nextDouble();
		
		System.out.println("What is your starting balance? ");
		initialBalance = keyboard.nextDouble();
		
		System.out.println("How many months have passed since you opened your account? ");
		months = keyboard.nextInt();

		SavingsAccount account = new SavingsAccount(initialBalance);
		
		account.rate(interestRate);
		
		while (months > 0)
		{
		System.out.println("What was deposit amount for month " + months + "? ");
		deposit = keyboard.nextDouble();
		account.deposit(deposit);
		
		System.out.println("What was withdrawal amount for month " + months + "? ");
		withdrawal = keyboard.nextDouble();
		account.withdraw(withdrawal);
		
		account.earned();
		months--;
		}
		
		System.out.println("This is your balance: $" + dollar.format(account.getbalance()));	
		System.out.println("This is your total deposits: $" + dollar.format(account.gettotalDeposits()));
		System.out.println("This is your total withdrawals: $" + dollar.format(account.gettotalWithdrawals()));
		System.out.println("This is your interest earned: $" + dollar.format(account.gettotalInterest()));	
	}
}

/**
   ----jGRASP exec: java AccountTest
    
    What is your annual interest rate? 
    12
    What is your starting balance? 
    100
    How many months have passed since you opened your account? 
    2
    What was deposit amount for month 2? 
    100
    What was withdrawal amount for month 2? 
    150
    What was deposit amount for month 1? 
    245
    What was withdrawal amount for month 1? 
    23
    This is your balance: $275.22
    This is your total deposits: $345.00
    This is your total withdrawals: $-173.00
    This is your interest earned: $3.22
    
     ----jGRASP: operation complete.
    
*/