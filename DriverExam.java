import java.util.Scanner;

public class DriverExam
{
		private String[] correctAns;
		private String[] studentAns;
		private boolean pass;
		
		public DriverExam (String[] a, String[] b)
		{	
			correctAns = a;
			studentAns = b;
			pass = false;
			
		}
		
		public boolean passed()
		{
			int  result = 0;
			for(int i = 0 ;  i  < 20;i++)
			{
				if(correctAns[i].equalsIgnoreCase(studentAns[i]))
					result++;
			}
	
		if( result >= 15) 
		return true;
		else 
		return false;

		}	

		public int totalCorrect()
		{
			int  result = 0;
			for(int i = 0 ;  i  < 20;i++)
			{
				if(correctAns[i].equalsIgnoreCase(studentAns[i]))
					result++;
			}
		
		return result;

		}	
		
		public int totalIncorrect()
		{
			int  result = 0;
			for(int i = 0 ;  i  < 20;i++)
			{
				if(correctAns[i].equalsIgnoreCase(studentAns[i])) 
					result++;
			}
		
		return 20-result;

		}
		
		public String questionsMissed()
		{
			String ansmiss = "";
			for(int i = 0 ;  i  < 20;i++)
			{
				if(!(correctAns[i].equalsIgnoreCase(studentAns[i]))) 
					ansmiss += " "+(i+1);
			}
		
		return ansmiss;

		}

}