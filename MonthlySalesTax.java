import javax.swing.*;    
import java.awt.event.*; 


public class MonthlySalesTaxWindow extends JFrame
{
   private JPanel panel;             
   private JLabel messageLabel;      
   private JTextField taxTextField; 
   private JButton calcButton;       
   private final int WINDOW_WIDTH = 310;  
   private final int WINDOW_HEIGHT = 100; 

   
   public MonthlySalesTaxWindow()
   {
      
      setTitle("Monthly Sales Tax Calculator");

      
      setSize(WINDOW_WIDTH, WINDOW_HEIGHT);

      
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

     
      buildPanel();

      
      add(panel);

      
      setVisible(true);
   }

   
   private void buildPanel()
   {
      
      messageLabel = new JLabel("Enter the amount to calculate");

      
      taxTextField = new JTextField(10);

      
      calcButton = new JButton("Calculate");

      
      calcButton.addActionListener(new CalcButtonListener());

      
      panel = new JPanel();

     
      panel.add(messageLabel);
      panel.add(kiloTextField);
      panel.add(calcButton);
   }

   

   private class CalcButtonListener implements ActionListener
   {
      

      public void actionPerformed(ActionEvent e)
      {
         String input;  
         double countyTax;  
			double stateTax;
        	double totalTax;
			
         input = taxTextField.getText();
         
        
         System.out.println("Reading " + input +
                            " from the text field.");
         System.out.println("Converted value: " +
                            Double.parseDouble(input));

         
         countyTax = Double.parseDouble(input) * 0.02;
			stateTax = Double.parseDouble(input) * 0.04;
			totalTax = countyTax + stateTax;
         
         JOptionPane.showMessageDialog(null,
                  " County sales tax is: " + countyTax + 
						"\nState sales tax is: " + stateTax +
						"\nTotal sales tax is: " + totalTax);

         
         System.out.println("Ready for the next input.");
      }
   } 

  

   public static void main(String[] args)
   {
      taxCalculatorWindow tc = new taxCalculatorWindow();
   }
}