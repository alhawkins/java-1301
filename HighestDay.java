public class HighestDay
{
public void getHighestDay()throws IOException
   {
      String testToken; 
  		double finalHighest = 0;    
		double highest = 0;   
		double nextHighest = 0;
		int week = 0;
		int weekCounter = 0;
		
		DecimalFormat fmt = new DecimalFormat("#,###.00");
      
		freader = new FileReader(filename);
      inputFile = new BufferedReader(freader);
		
		line = inputFile.readLine();
				
		System.out.println();
		outputFile.println();
		
		while(line != null)
		{
			
				StringTokenizer tokenizer = new StringTokenizer(line, ",");
					  
			   while (tokenizer.hasMoreTokens())
    			{
     				testToken = tokenizer.nextToken();
         		highest = Double.parseDouble(testToken);  			
								
					if (highest > nextHighest)  
      			{
						nextHighest = highest;
						weekCounter++;
						week = weekCounter;				
					}
					else
						weekCounter++;
						
				}

		finalHighest = nextHighest;			
		line = inputFile.readLine();
		}
		
		JOptionPane.showMessageDialog(null,"Day " + week + " had the highest sales with $"+ fmt.format(finalHighest));
		outputFile.println("Day " + week + " had the highest sales with $"+ fmt.format(finalHighest));
		

		inputFile.close();  
	}
}