import java.util.Scanner;

public class TestGradeBook 
{

		public static void main(String[] args)
		{
				char[] grade = new char[5];
				String[] name = new String[5];
				double[] average = new double[5];
								
				GradeBook run = new GradeBook();
				
				run.getNames();		
				run.getScores();
						
				
				name = run.showNames();
				average = run.averageRow();
				grade = run.letterGrade();
				
				System.out.println("");
					
					for (int i = 0; i<5; i++)
					{
							System.out.println(name[i]+" as an average of "+average[i]+" with a letter grade "+grade[i]);
							System.out.println("");
					}
				
		}
			
		
}

/**
   ----jGRASP exec: java TestGradeBook
    Enter the name of student 1: 
    Allan
    Enter the name of student 2: 
    Bob
    Enter the name of student 3: 
    Jill
    Enter the name of student 4: 
    Janet
    Enter the name of student 5: 
    Sam
    
    Enter Allan's score for test 1: 
    100
    Enter Allan's score for test 2: 
    100
    Enter Allan's score for test 3: 
    100
    Enter Allan's score for test 4: 
    100
    
    Enter Bob's score for test 1: 
    90
    Enter Bob's score for test 2: 
    90
    Enter Bob's score for test 3: 
    90
    Enter Bob's score for test 4: 
    90
    
    Enter Jill's score for test 1: 
    80
    Enter Jill's score for test 2: 
    80
    Enter Jill's score for test 3: 
    80
    Enter Jill's score for test 4: 
    80
    
    Enter Janet's score for test 1: 
    70
    Enter Janet's score for test 2: 
    70
    Enter Janet's score for test 3: 
    70
    Enter Janet's score for test 4: 
    70
    
    Enter Sam's score for test 1: 
    60
    Enter Sam's score for test 2: 
    60
    Enter Sam's score for test 3: 
    60
    Enter Sam's score for test 4: 
    60
    
    Allan as an average of 100.0 with a letter grade A
    
    Bob as an average of 90.0 with a letter grade A
    
    Jill as an average of 80.0 with a letter grade B
    
    Janet as an average of 70.0 with a letter grade C
    
    Sam as an average of 60.0 with a letter grade D
*/