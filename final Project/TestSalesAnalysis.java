import java.io.*;  
import java.text.DecimalFormat;

/**
Author: Allan Hawkins 
class: 1301, Prof. Bota 
1301 final project
*/

/**
Program that tests the SalesAnalysis class
*/

public class TestSalesAnalysis
{
   public static void main(String[] args)throws IOException
   {
              
      SalesAnalysis data = new SalesAnalysis("SalesData.txt", "SalesAnalysys.txt");
      
  		data.getTotalWeek();
      data.getDailyWeekAverage();      
		data.getTotalWeeks();
		data.getWeekAverage();
		data.getHighestWeek();
		data.getLowestWeek();
		data.close();
      
   }
}
