


public class TestParkingTicket
{
   public static void main(String[] args)
   {
      
      ParkedCar pCar = new ParkedCar(CarType.FERRARI, CarModel.SPIDER, CarColor.RED, "ct3789", 300);
		ParkingMeter pMinutes = new ParkingMeter(30);
		PoliceOfficer officer = new PoliceOfficer("Smith", "John", "12345");
		ParkingTicket ticket = new ParkingTicket(pCar, pMinutes, officer);
		officer.expired(pCar, pMinutes, ticket);
				
      
	}
}