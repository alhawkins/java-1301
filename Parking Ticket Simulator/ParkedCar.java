


public class ParkedCar
{
   private CarType type;    
 	private CarModel model;  
	private CarColor color;  
   private String plateNum;    
  	private int minutes;
     
   public ParkedCar(CarType t, CarModel m, CarColor c,
                    String p, int a)
   {
      type = t;
  		model = m;    
		color = c;
      plateNum = p;
		minutes = a;
   }

 	public ParkedCar(ParkedCar object2)
	{
		type = object2.type;
  		model = object2.model;    
		color = object2.color;
      plateNum = object2.plateNum;
		minutes = object2.minutes;
	}    
  
	public int getMinutes()
	{
		return minutes;
	}  
	
   public String toString()
   {
      String str = "Make:                   " + type + 
       				 "\nModel:                  " + model +            
						 "\nColor:                  " + color +
                   "\nPlate #:                " + plateNum+
						 "\nMinutes parked:         " + minutes;
      				
     
      return str;
   }
}
