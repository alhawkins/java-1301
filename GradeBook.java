import java.util.Scanner;

public class GradeBook 
{
		
		private String[] names = new String[5];
		private double[][] scores = new double[5][4];
		private String[] grades;	
		private double[] sumRow = new double[5];		
		
		Scanner keyboard = new Scanner(System.in);					
	
		public void getNames()
		{
			for(int i = 0; i < 5; i++)
				{
				System.out.println("Enter the name of student "+(i+1)+": ");
				names[i] = keyboard.nextLine();
				}
		}
		
		public void getScores()
		{
				for (int row = 0; row < 5; row++)
				{
						System.out.println();
						for (int col = 0; col < 4; col++)
						{
								System.out.println("Enter "+names[row]+ "'s score for test " + (col + 1) + ": ");
								scores[row][col] = keyboard.nextInt();
									
								if (scores[row][col] > 100)
								{
									System.out.println("Invalin number, Please re-enter the score: ");
									scores[row][col] = keyboard.nextInt();
								}
								if (scores[row][col] < 0)
								{
									System.out.println("Invalin number, Please re-enter the score: ");
									scores[row][col] = keyboard.nextInt();
								}

						}
				
				}
		
		}
		public String[] showNames()
		{		
				String[] name = new String[5];
				
				for (int i = 0; i<5; i++)
						{
								
								name[i] = names[i];
						
						}

				return name;
		}

		public double[] averageRow()
		{		
			double sum;
			
		
			for ( int i = 0; i<5; i++)
			{
					sum = 0;
						for (int k = 0; k<4; k++)
						{
								sum += scores[i][k];
								sumRow[i] = sum/4;
						
						}
			
			}							
											
			return sumRow; 
		}
		
		public char[] letterGrade()
		{	 
		 	char[] letter = new char[5];	
			double[] ave = averageRow();
			
				
				for (int i = 0; i<5; i++)
						{
							if (ave[i]>=90)
								letter[i] = 'A';
							else if (ave[i] >= 80)
								letter[i] = 'B';	
							else if (ave[i] >= 70)
								letter[i] = 'C';	
							else if (ave[i] >= 60)
								letter[i] = 'D';
							else if (ave[i] < 60)
								letter[i] = 'B';						
						}

				return letter;

		}		


}