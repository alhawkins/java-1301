

public class ParkingMeter
{
		private int paidMinutes;
		
		public ParkingMeter()
		{
				paidMinutes = 0;
		}
		public ParkingMeter(int p)
		{
				paidMinutes = p;
		}
		
		public int getPaidMinutes()
		{
			return paidMinutes;
		}
		
		public ParkingMeter(ParkingMeter object2)
		{
				paidMinutes = object2.paidMinutes;
		}		
		
		public String toString()
  		{
      String str = "Purchased Minutes:      " + paidMinutes;
       				      
      return str;
  		}
}