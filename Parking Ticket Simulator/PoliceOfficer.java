
public class PoliceOfficer
{
   private String lastName;        
   private String firstName;    
   private String badgeNum; 
	private ParkedCar parkedCar;     
	private ParkingTicket parkingticket;
	private int difMinutes;
	   
 	  
	public PoliceOfficer(String lname, String fname, String num)
   {
      lastName = lname;
      firstName = fname;
      badgeNum = num;
		
   }
 	 	
	public void expired(ParkedCar car, ParkingMeter meter, ParkingTicket ticket)
   {
      difMinutes = car.getMinutes() - meter.getPaidMinutes();
		ticket.fine(difMinutes);
		
		if(car.getMinutes() > meter.getPaidMinutes())
		{
			
			System.out.println(ticket);
				
		}
		else if (car.getMinutes() < meter.getPaidMinutes())
		{
			System.out.println("Time has not expired");
			
		}
		
	}
	
	public PoliceOfficer(PoliceOfficer object2)
   {
      lastName = object2.lastName;
      firstName = object2.firstName;
      badgeNum = object2.badgeNum;
		difMinutes = object2.difMinutes;
	}

			
		
	      
   public String toString()
   {
      
      String str = "Officer's Last Name:    " + lastName +
                   "\nOfficer's First Name:   " + firstName +
                   "\nOfficer's Badge Number: " + badgeNum;

     
      return str;
   }
}