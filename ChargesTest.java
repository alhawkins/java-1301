import java.util.Scanner;
import java.text.DecimalFormat;

public class ChargesTest
{

	public static void main(String[] args)
	{
		double initialBalance;
		int checks;
		
		DecimalFormat dollar = new DecimalFormat("#,###.00");
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("What is your starting balance? ");
		initialBalance = keyboard.nextDouble();
		
		System.out.println("How many checks have you written? ");
		checks = keyboard.nextInt();
		
		BankCharges charges = new BankCharges(initialBalance, checks);
		
		charges.extraFee(initialBalance);
		
		charges.fees(checks);
		
		System.out.println("Your monthly fees are:$" + dollar.format(charges.getFees()));
		
	}
}

/**
   
     ----jGRASP exec: java ChargesTest
    
    What is your starting balance? 
    200
    How many checks have you written? 
    23
    Your monthly fees are:$26.84
    
     ----jGRASP: operation complete.
    
*/