public class SavingsAccount
{ 
	private double balance; 
	private double totalDeposits;
	private double totalWithdrawals;
	private double totalInterest;
	private double monthlyRate;
	
	public SavingsAccount()
	{
	
		balance = 0.0;
	}

	public SavingsAccount(double initialBalance)
	{
		
		balance = initialBalance;
	}

	public SavingsAccount(String iR, String iB)
	{
		
		balance = Double.parseDouble(iB);
	}

	public void deposit(double amount)
	{
		balance += amount;
		totalDeposits += amount;
	}

	public void deposit(String amount)
	{
		balance += Double.parseDouble(amount);
		totalDeposits += Double.parseDouble(amount);
	}

	public void withdraw(double amount)
	{
		balance -= amount;
		totalWithdrawals -= amount;
	}
	
	public void withdraw(String amount)
	{
		balance -= Double.parseDouble(amount);
		totalWithdrawals -= Double.parseDouble(amount);
	}

	public void rate(double interestRate)
	{
		double r;
		r = interestRate/12;	
		monthlyRate = r/100; 
			
	}
	
		public void earned()
	{
		double i;
		i = monthlyRate * balance;
		totalInterest += i;
		balance += i;	
	}

	public void setBalance(double b)
	{
		balance = b;
	}

	public void setBalance(String b)
	{
		balance = Double.parseDouble(b);
	}
	
	public double getbalance()
	{
		return balance;
	}

		public double gettotalDeposits()
	{
		return totalDeposits;
	}

		public double gettotalWithdrawals()
	{
		return totalWithdrawals;
	}
	
		public double gettotalInterest()
	{
		return totalInterest;
	}
	
}

