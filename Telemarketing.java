import java.util.Scanner;


public class Telemarketing
{
   public static void main(String[] args)
   {
      String lookUp; 
      
      
      String[] people = { "Harrison, Rose:","James, Jean:","Smith, Williams:","Smith, Brad:" };
		String[] numbers = { " 555-2234"," 555-9098"," 555-1785"," 555-9224"};
		
      Scanner keyboard = new Scanner(System.in);
      
      System.out.print("Enter the first few characters of " +
                       "the last name to look up: ");
      lookUp = keyboard.nextLine();

      System.out.println("Here are the names that match:");  
      
		for (int i = 0; i < people.length; i++)
      {
         if (people[i].startsWith(lookUp))
    		{	      
				System.out.print(people[i]);
  				System.out.println(numbers[i]);
			}    
		}
   }
}
/**
   ----jGRASP exec: java Telemarketing
    Enter the first few characters of the last name to look up: Sm
    Here are the names that match:
    Smith, Williams: 555-1785
    Smith, Brad: 555-9224
    
     ----jGRASP: operation complete.
    
*/