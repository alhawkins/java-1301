import java.util.Scanner;

public class TestDriverExam
{
		public static void main(String[] arg)
		{
				String[] correctAns = {"B","D","A","A","C","A","B","A","C","D","B",
												"C","D","A","D","C","C","B","D","A"};	
				final int answers = 20;
				String input;
				
				String[] studentAns = new String[answers];
				
				Scanner keyboard = new Scanner(System.in);
				
				for (int i = 0; i < 20; i++)
				{
				System.out.println("Enter the student's answer for question #" + (i + 1) +":");
				input = keyboard.nextLine();
				
				if (input.equals("A")||input.equals("B")||input.equals("C")||input.equals("D"))
					{
					studentAns[i] = input;
					}		
				else
					{
					System.out.println("\nInvalid Answer. Use Capital letters.\n");
					i--;
					}
				
				}
				
				DriverExam test = new DriverExam(correctAns, studentAns);	
				
				if(test.passed())
					System.out.println("You passed");
				else
					System.out.println("You failed");
				System.out.println("You had " + test.totalCorrect()+" correct answers");
				System.out.println("You had " + test.totalIncorrect()+" incorrect answers");
				System.out.println("You missed questions"+ test.questionsMissed());
										
				
		
		}

}
/**
    ----jGRASP exec: java TestDriverExam
    Enter the student's answer for question #1:
    B
    Enter the student's answer for question #2:
    C
    Enter the student's answer for question #3:
    A
    Enter the student's answer for question #4:
    A
    Enter the student's answer for question #5:
    D
    Enter the student's answer for question #6:
    A
    Enter the student's answer for question #7:
    B
    Enter the student's answer for question #8:
    A
    Enter the student's answer for question #9:
    C
    Enter the student's answer for question #10:
    D
    Enter the student's answer for question #11:
    B
    Enter the student's answer for question #12:
    C
    Enter the student's answer for question #13:
    D
    Enter the student's answer for question #14:
    A
    Enter the student's answer for question #15:
    D
    Enter the student's answer for question #16:
    A
    Enter the student's answer for question #17:
    A
    Enter the student's answer for question #18:
    B
    Enter the student's answer for question #19:
    D
    Enter the student's answer for question #20:
    A
    You passed
    You had 16 correct answers
    You had 4 incorrect answers
    You missed questions 2 5 16 17
*/